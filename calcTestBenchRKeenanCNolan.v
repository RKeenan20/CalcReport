`timescale 1ns / 1ns

module calcCoreLogicTB;

	// Logic Module inputs
	reg clk, rst, newkey;
	reg [4:0] keycode;


	// Outputs from our Calculator Core Logic Module under test
	wire [15:0] valueOutput;
	wire overflowLED;


	// Defining Function Keys - Algebraic Operators + Special Functions
	localparam [4:0] PLUS = 5'h19, MINUS = 5'h1B, MULT = 5'h1A, EQUALS = 5'h14, CE = 5'h11, CA = 5'h13, SQUARE = 5'h1C, DELETE=5'h12;

	//Defining local parameters for A-F of hexadeicmal for clarity in the button press cases/Simplicity of Entry
	localparam [4:0] A = 5'hA, B = 5'hB, C = 5'hC, D = 5'hD, E = 5'hE, F=5'hF;

	localparam CONSOLE = 1;  //Console output

	// Testbench internal variables

	integer errorCount = 0;
	integer outFile;

	// Core Logic Instantiation
	calcCoreLogic UUT (
        .clock(clk),
        .reset(rst),
        .keycode(keycode),
        .newkey(newkey),
        .valueOutput(valueOutput),
        .overflowLED(overflowLED)
        );


// Generate the 5 MHz clock signal
	initial begin
		clk = 0;		// initialise clock
		#100;				// delay at start
		forever
		  #100 clk = ~clk;		// delay 100 ns and invert the clock
	end

// Define the test sequence
	initial begin
		outFile = $fopen("calcCoreLogic_log.txt"); // open the log file for output of results

		rst = 1'b0;			// initialise all the inputs
		keycode = 5'b0;
		newkey = 1'b0;

		#100;   						// delay before reset, so ouptut can be seen
		rst = 1'b1;  				// reset of at least one clock cycle
		@(negedge clk);	    // wait for falling clock edge
		@(negedge clk) rst = 1'b0;		// end pulse at second falling edge

		#200;		    //Further delay to analyse effect of reset


//Test Cases from Verification Plan shown below to test all aspects

		//CASE 1 - Simple Addition (2+3 = 0005)
    PRESS(2);        //First digit pressed
    CHECK(16'h2);    //Check 2 is diaplayed to screen
    PRESS(PLUS);     // + Operator
    CHECK(16'h2);    //Checking 2 remains of the screen after button press
    PRESS(3);        //Second digit
    CHECK(16'h3);    //Check 3 is displayed to screen
    PRESS(EQUALS);   //Press equals
    CHECK(16'h5);    //Check 15 bit hexadeicmal 5 is displayed


		//CASE 2 - Simple Multiplication (2x6 = 000C)
		PRESS(CA); //Clear ALL
		CHECK(16'h0);  //Checking Display has been cleared
		PRESS(2);
		CHECK(16'h2);
		PRESS(MULT);
		CHECK(16'h2);
		PRESS(6);
		CHECK(16'h6);
		PRESS(EQUALS);
		CHECK(16'hC);


		//CASE 3 - Simple Subtraction (4 - 2 = 0002)
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		CHECK(16'h4);
		PRESS(MINUS);
		CHECK(16'h4);
		PRESS(2);
		CHECK(16'h2);
		PRESS(EQUALS);
		CHECK(16'h2);


		//CASE4 - Larger Operands - First Computation: (332 + 1234 = 1566)
		PRESS(CA);
		CHECK(16'h0);
		PRESS(3);
		CHECK(16'h3);
		PRESS(3);
		CHECK(16'h33); //Checking 33
		PRESS(2);
		CHECK(16'h332); //Checking 332
		PRESS(PLUS);
		CHECK(16'h332); //Checking 332 stays on display
		PRESS(1);
		CHECK(16'h1);
		PRESS(2);
		CHECK(16'h12);
		PRESS(3);
		CHECK(16'h123);
		PRESS(4);
		CHECK(16'h1234);
		PRESS(EQUALS);
		CHECK(16'h1566);
		PRESS(CE); //Clearing Xregister
		CHECK(16'h0);

		//next computation of part 4 - (500*24 = B40)
		PRESS(2);
		CHECK(16'h2);
		PRESS(4);
		CHECK(16'h24);
		PRESS(MULT);
		CHECK(16'h24);
		PRESS(5);
		CHECK(16'h5);
		PRESS(0);
		CHECK(16'h50);
		PRESS(0);
		CHECK(16'h500);
		PRESS(EQUALS);
		CHECK(16'hB400);
		//CHECKOVF(Xdisplay);

		//final computation (530-500 = 30)
		PRESS(CA);
		CHECK(16'h0);
		PRESS(5);
		CHECK(16'h5);
		PRESS(3);
		CHECK(16'h53);
		PRESS(0);
		CHECK(16'h530);
		PRESS(MINUS);
		CHECK(16'h530);
		PRESS(5);
		CHECK(16'h5);
		PRESS(0);
		CHECK(16'h50);
		PRESS(0);
		CHECK(16'h500);
		PRESS(EQUALS);
		CHECK(16'h30);


		//CASE5 - Reuse of result
		//first calculation - (4*3 = +5 = 11)

		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		CHECK(16'h4);
		PRESS(MULT);
		CHECK(16'h4);
		PRESS(3);
		CHECK(16'h3);
		PRESS(EQUALS);
		CHECK(16'hC);
		PRESS(PLUS);
		CHECK(16'hC);
		PRESS(5);
		CHECK(16'h5);
		PRESS(EQUALS);
		CHECK(16'h11);

		//second calculation (12+12 = +6 = 24)
		PRESS(1);
		CHECK(16'h1);
		PRESS(2);
		CHECK(16'h12);
		PRESS(PLUS);
		CHECK(16'h12);
		PRESS(1);
		CHECK(16'h1);
		PRESS(2);
		CHECK(16'h12);
		PRESS(EQUALS);
		CHECK(16'h24);
		PRESS(PLUS);
		CHECK(16'h24);
		PRESS(6);
		CHECK(16'h6);
		PRESS(EQUALS);
		CHECK(16'h2A);

		//CASE 6 - Squared function of Small and Large Numbers

		//40^2 = 1000
    PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		PRESS(0);
		PRESS(SQUARE);
		CHECK(16'h1000);



		//6^2 = 0024
		PRESS(CA);
		CHECK(16'h0);
		PRESS(6);
		PRESS(SQUARE);
		CHECK(16'h0024);

		//FF^2 = FE01
		PRESS(CA);
		CHECK(16'h0);
		PRESS(F);
		PRESS(F);
		PRESS(SQUARE);
		CHECK(16'hFE01);

		//CASE 7 - Chain Operations
		//2+4+5+6 = 0011
		PRESS(CA);
		CHECK(16'h0);
		PRESS(2);
		CHECK(16'h2);
		PRESS(PLUS);
		CHECK(16'h2);
		PRESS(4);
		CHECK(16'h4);
		PRESS(PLUS);
		CHECK(17'h6);
		PRESS(5);
		CHECK(16'h5);
		PRESS(PLUS);
		CHECK(16'hB);
		PRESS(6);
		CHECK(16'h6);
		PRESS(EQUALS);
		CHECK(16'h11);

		//4^2 + 2 = 0012
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		CHECK(16'h4);
		PRESS(SQUARE);
		CHECK(16'h10);
		PRESS(PLUS);
		CHECK(16'h10);
		PRESS(2);
		CHECK(16'h2);
		PRESS(EQUALS);
		CHECK(16'h12);

		//2^2 * 4 = 10
		PRESS(CA);
		CHECK(16'h0);
		PRESS(2);
		CHECK(16'h2);
		PRESS(SQUARE);
		CHECK(16'h4);
		PRESS(MULT);
		CHECK(16'h4);
		PRESS(4);
		CHECK(16'h4);
		PRESS(EQUALS);
		CHECK(16'h10);

		//CASE 8 - Clear On Entry 2+3=0005 -> Pressing 2 after yields only a 2 on screen
		PRESS(CA);
		CHECK(16'h0);
		PRESS(2);
		CHECK(16'h2);
		PRESS(PLUS);
		CHECK(16'h2);
		PRESS(3);
		CHECK(16'h3);
		PRESS(EQUALS);
		CHECK(16'h5);
		PRESS(2);
		CHECK(16'h2);

		//CASE 9 - Clear Entry
		//single digit CE: 2
		PRESS(CA);
		CHECK(16'h0);
		PRESS(2);
		CHECK(16'h2);
		PRESS(CE);
		CHECK(16'h0);

		//large number CE: 332A
		PRESS(3);
		PRESS(2);
		PRESS(2);
		CHECK(16'h322);
		PRESS(A);
		CHECK(16'h322A);
		PRESS(CE);
		CHECK(16'h0);

		//44 CE 6 + 3 = 0009
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		CHECK(16'h4);
		PRESS(4);
		CHECK(16'h44);
		PRESS(CE);
		CHECK(16'h0);
		PRESS(6);
		CHECK(16'h6);
		PRESS(PLUS);
		CHECK(16'h6);
		PRESS(3);
		CHECK(16'h3);
		PRESS(EQUALS);
		CHECK(16'h9);

		//44 + 6 CE 3 = 00
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		CHECK(16'h4);
		PRESS(4);
		CHECK(16'h44);
		PRESS(PLUS);
		CHECK(16'h44);
		PRESS(6);
		CHECK(16'h6);
		PRESS(CE);
		CHECK(16'h0);
		PRESS(3);
		CHECK(16'h3);
		PRESS(EQUALS);

		//CASE 10 - Multiple Digits on the Display
		PRESS(CA);
		CHECK(16'h0);
		PRESS(1);
		CHECK(16'h1);
		PRESS(2);
		CHECK(16'h12);
		PRESS(3);
		CHECK(16'h123);
		PRESS(4);
		CHECK(16'h1234);
		PRESS(5);
		CHECKOVF(1);
		CHECK(16'h1234);


		//CASE 11 - Clear All
		//2 + 3 CA = 0
		PRESS(CA);
		CHECK(16'h0);
		PRESS(2);
		PRESS(PLUS);
		CHECK(16'h2);
		PRESS(3);
		CHECK(16'h3);
		PRESS(CA);
		CHECK(16'h0);
		PRESS(EQUALS);
		CHECK(16'h0);

		//ABCD * 4444 CA = 0000
		PRESS(A);
		PRESS(B);
		PRESS(C);
		PRESS(D);
		PRESS(MULT);
		CHECK(16'hABCD);
		PRESS(4);
		PRESS(4);
		PRESS(4);
		PRESS(4);
		CHECK(16'h4444);
		PRESS(CA);
		CHECK(16'h0);
		PRESS(EQUALS);
		CHECK(16'h0);

		// 1234+234 = 1468 CA *10 = 0000
		PRESS(1);
		PRESS(2);
		PRESS(3);
		PRESS(4);
		CHECK(16'h1234);
		PRESS(PLUS);
		CHECK(16'h1234);
		PRESS(2);
		PRESS(3);
		PRESS(4);
		CHECK(16'h234);
		PRESS(EQUALS);
		CHECK(16'h1468);
		PRESS(CA);
		CHECK(16'h0);
		PRESS(MULT);
		CHECK(16'h0);
		PRESS(1);
		PRESS(0);
		CHECK(16'h10);
		PRESS(EQUALS);
		CHECK(16'h0);

		//CASE 12 - Delete Digit Function
		// 4 Delete
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		CHECK(16'h4);
		PRESS(DELETE);
		CHECK(16'h0);

		//1234 DEL 0123 DEL 0012 DEL 0001 DEL 0000
		PRESS(CA);
		CHECK(16'h0);
		PRESS(1);
		PRESS(2);
		PRESS(3);
		PRESS(4);
		CHECK(16'h1234);
		PRESS(DELETE);
		CHECK(16'h123);
		PRESS(DELETE);
		CHECK(16'h12);
		PRESS(DELETE);
		CHECK(16'h1);
		PRESS(DELETE);
		CHECK(16'h0);

		// 4 + 33, DEL = 0007
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		PRESS(PLUS);
		CHECK(16'h4);
		PRESS(3);
		PRESS(3);
		CHECK(16'h33);
		PRESS(DELETE);
		CHECK(16'h3);
		PRESS(EQUALS);
		CHECK(16'h7);

		//44, DEL, +, 3 = 0007
		PRESS(4);
		PRESS(4);
		PRESS(DELETE);
		CHECK(16'h4);
		PRESS(PLUS);
		PRESS(3);
		CHECK(16'h3);
		PRESS(EQUALS);
		CHECK(16'h7);


		// 4+33, DEL, 4, = 0038
		PRESS(CA);
		CHECK(16'h0);
		PRESS(4);
		PRESS(PLUS);
		CHECK(16'h4);
		PRESS(3);
		PRESS(3);
		CHECK(16'h33);
		PRESS(DELETE);
		CHECK(16'h3);
		PRESS(4);
		CHECK(16'h34);
		PRESS(EQUALS);
		CHECK(16'h38);

		//TASK 13 - Overflow Warning - As per specification of a 4 digit display: Computations resulting in overflow are tested here

		//FFFF+1234 - Overflow = 1
		PRESS(CA);
		CHECK(16'h0);
		PRESS(F);
		PRESS(F);
		PRESS(F);
		PRESS(F);
		CHECK(16'hFFFF);
		PRESS(PLUS);
		CHECK(16'hFFFF);
		PRESS(1);
		PRESS(2);
		PRESS(3);
		PRESS(4);
		CHECK(16'h1234);
		PRESS(EQUALS);
		CHECKOVF(1);

		//FFBA + 1234 - Overflow = 1
		PRESS(CA);
		CHECK(16'h0);
		PRESS(F);
		PRESS(F);
		PRESS(B);
		PRESS(A);
		PRESS(PLUS);
		CHECK(16'hFFBA);
		PRESS(1);
		PRESS(2);
		PRESS(3);
		PRESS(4);
		CHECK(16'h1234);
		PRESS(EQUALS);
		CHECKOVF(1);

		//2-4 OVF - Overflow = 1
		PRESS(CA);
		CHECK(16'h0);
		PRESS(2);
		CHECK(16'h2);
		PRESS(MINUS);
		CHECK(16'h2);
		PRESS(4);
		CHECK(16'h4);
		PRESS(EQUALS);
		CHECKOVF(1);

		// FFBA - FFFF = Overflow = 1
		PRESS(CA);
		CHECK(16'h0);
		PRESS(F);
		PRESS(F);
		PRESS(B);
		PRESS(A);
		CHECK(16'hFFBA);
		PRESS(MINUS);
		CHECK(16'hFFBA);
		PRESS(F);
		PRESS(F);
		PRESS(F);
		PRESS(F);
		PRESS(EQUALS);
		CHECKOVF(1);

		//FFAB*FFBB - Overflow = 1
		PRESS(CA);
		CHECK(16'h0);
		PRESS(F);
		PRESS(F);
		PRESS(A);
		PRESS(B);
		CHECK(16'hFFAB);
		PRESS(MULT);
		CHECK(16'hFFAB);
		PRESS(F);
		PRESS(F);
		PRESS(B);
		PRESS(B);
		CHECK(16'hFFBB);
		PRESS(EQUALS);
		CHECKOVF(1);

		//Checking for contact bounce and whether our calculator ignores it
    #600
    newkey = 1'b0;
    rst = 1'b0;
    keycode = 5'b0;
    #150
    rst = 1'b1;
    #300
    rst = 1'b0;
    #600
    keycode = 5'b10010; //Simulating bounce of the 2 key, newkey is still 0 so keycode will remain at prev val (0000)
    CHECK(16'h0);
    #600
    keycode = 5'b00000;
    #600
    keycode = 5'b10010;
    #600
    CHECK(16'h0);
    keycode = 5'b10010;
    #700
    newkey = 1'b1; //newkey when keycode is stable
    CHECK(16'h2);
    #30
    newkey = 1'b0;
    #900
    keycode = 5'b00000; //simulate bounce on button release
    CHECK(16'h2);
    #700
    keycode = 5'b10010;
    #600
    keycode = 5'b00000;

		#600;    //delay before closing file
		$fdisplay(outFile|CONSOLE, "Behavioural Simulation Complete - Error Count: %d",errorCount);
		$fclose(outFile);  // closing output file
		$stop;			// end of sim
	end


/*  Task to simulate the push of a keypad button. To conform to hardware spec, the inputs to the caclulator logic act on the negative edge of the clock  */
	task PRESS (input [4:0] keycodeTB);
        begin
            @ (posedge clk);	//act on clock edge
			#1 keycode = keycodeTB ^ 5'h10;	// Since digit keys are defined keycode[5] = 1 , it is necessary that the MSB be inverted for clarity, for key presses within the testbench
			@ (posedge clk);
			#1 newkey = 1'b1;	// set newkey to 1 just after next clock edge
			@ (posedge clk);
			#1 newkey = 1'b0;   //retunr newkey to zero after one clock cycle
			// log the keypress in the output file
			$fdisplay(outFile, " time %t ps, key pressed: %h", $time, keycode);
			repeat(5)
				@ (posedge clk);    // keep keycode active for 5 more clock cycles for clarity on timing diagram
			#1 keycode = 5'h0;	    // Replicating depress of button
		end
	endtask

/*  Task to check the output from the calculator.  Input to the task is the expected value of the calculator, which is checked on the
     falling edge of the clock, when it should be stable. Both errors and outputs are passed to the output file. Errors are also displayed in the
     console  */
	task CHECK (input [15:0] expectedVal);
        begin
            @ (negedge clk);	// wait for falling edge of clock
            if (valueOutput != expectedVal)
                begin   // error message to log file and to console
                    $fdisplay(outFile|CONSOLE, "*Error: %t ps, valueOutput = %h, expectedVal = %h", $time, valueOutput, expectedVal);
                    errorCount = errorCount + 1;	// increment error counter
                end
            else  // Recording non-error checks in file
                $fdisplay(outFile, " time %t ps, Output = %h", $time, valueOutput);
        end
    endtask

	/*Task to check the overflowLED from the calculator.*/
	task CHECKOVF(input OVF);
  	     begin
            @(negedge clk);
          	if(overflowLED != OVF) // If overflow does not equal 1 or 0 (when expected) - give error message
                begin
                    errorCount= errorCount+1; //increase error count
                    $fdisplay(outFile|CONSOLE, "*Error: time %t ps, Overflow = %h, expected %h", $time, overflowLED, OVF); //send to output file and console
                end
						else
			    			$fdisplay(outFile, " time %t ps, Overflow = %h", $time, overflowLED);
          end
  endtask

endmodule
