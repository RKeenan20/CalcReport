//////////////////////////////////////////////////////////////////////////////////
// Engineers:      Robert Keenan & Ciaran Nolan
// Target Device: XC7A100T-csg324 on Digilent Nexys 4 board
// Description:   Top-level module for calculator design.
//                Defines top-level input and output signals.
//                Instantiates clock and reset generator block, for 5 MHz clock
//
//
//  Created: 18 November 2017
//////////////////////////////////////////////////////////////////////////////////
module calculator_top(
        input clk100,		 // 100 MHz clock from oscillator on board
        input rstPBn,		 // reset signal, active low, from CPU RESET pushbutton
        input [5:0] kpcol,   // keypad column signals
        output [3:0] kprow,  // keypad row signals
        output [7:0] digit,  // digit controls - active low (7 on left, 0 on right)
        output [7:0] segment, // segment controls - active low (a b c d e f g dp)
        output overflowLED);

// ===========================================================================
// Interconnecting Signals
    wire clk5;              // 5 MHz clock signal, buffered
    wire reset;             // internal reset signal, active high
    wire newkey;            // 1 clock cycle long pulse to indicate new key pressed, keycode valid
    wire [4:0] keycode;     // 5-bit code to identify key pressed
    wire [15:0] calcOut;    // 16-bit output from calculator, to be displayed


// ===========================================================================
// Instantiate clock and reset generator, connect to signals
    clockReset  clkGen  (
            .clk100 (clk100),
            .rstPBn (rstPBn),
            .clk5   (clk5),
            .reset  (reset) );

//==================================================================================
// Calculator Core logic - Instantiation of our calculator core logic
	calcCoreLogic myCalc1 (
        .clock(clk5),
        .reset(reset),
        .keycode(keycode),
        .newkey(newkey),
        .valueOutput(calcOut),
        .overflowLED(overflowLED)
        );

//==================================================================================
// Keypad interface to scan keypad and return valid keycodes
    keypad keyp1 (
        .clk(clk5),            // clock for keypad module is 5 MHz
        .rst(reset),            // reset is internal reset signal
        .kpcol(kpcol),            // 6 keypad column inputs
        .kprow(kprow),            // 4 keypad row outputs
        .newkey(newkey),        // new key signal
        .keycode(keycode)        // 5-bit code representing key
        );

//==================================================================================
// Display interface, for 4 digits
	DisplayInterface disp1 (
           .clk5(clk5),
	       .reset(reset),
	       .dispVal(calcOut),
	       .point(4'b0000),    // set all point markers to off
		   .digit(digit[3:0]), // only using rightmost 4 digits
		   .segment(segment)
		   );

	assign digit[7:4] = 4'b1111;   // turn off unused digits

endmodule
